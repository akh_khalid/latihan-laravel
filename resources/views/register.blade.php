@extends('layout.master')

@section('judul')
<h1>Register</h1>
@endsection



@section('content')
 
<h1>Buat Account Baru!</h1>

<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
@csrf
    <label for="fname">First name:</label><br><br>
    <input type="text" id="fname" name="fname"><br><br>
    <label for="lname">Last name:</label><br><br>
    <input type="text" id="lname" name="lname">
<br>
<p>Gender:</p>
  <input type="radio" id="male" name="fav_language" value="Male">
  <label for="male">Male</label><br>
  <input type="radio" id="female" name="fav_language" value="Female">
  <label for="female">Female</label><br>
  <input type="radio" id="other" name="fav_language" value="Other">
  <label for="other">Other</label>

<br>
<br>  
<label for="nationality">Nationality:</label>
<br><br>
  <select name="nationality" id="nationality">
    <option value="indonesia">Indonesia</option>
    <option value="amerika">Amerika</option>
    <option value="inggris">Inggris</option>
  </select>

<br>
<p>Language Spoken:</p>
  <input type="checkbox" id="bahasa1" name="bahasa1" value="Bahasa Indonesia">
  <label for="bahasa1"> Bahasa Indonesia</label><br>
  <input type="checkbox" id="bahasa2" name="bahasa2" value="English">
  <label for="bahasa2"> English</label><br>
  <input type="checkbox" id="bahasa3" name="bahasa3" value="Other">
  <label for="bahasa3"> Other</label>
  
<br>
 <p>Bio:</p>  
  <textarea name="bio" rows="10" cols="30">
    </textarea>
  <br>  
  <input type="submit" value="Kirim">

</form>
@endsection